class Order:
    def __init__(self, customer_email):
        self.customer_email = customer_email
        self.products = []
        self.purchased = False

    def add_product(self, product):

        self.products.append(product)

    def get_total_price(self):

        prices = []

        for n in self.products:
            prices.append(n.get_price())

        return sum(prices)

    def get_total_quantity_of_products(self):

        quantities = 0

        for n in self.products:
            quantities += n.quantity

        return quantities

    def purchase(self):

        self.purchased = True


class Product:

    def __init__(self, name, unit_price, quantity=1):
        self.name = name
        self.unit_price = unit_price
        self.quantity = quantity

    def get_price(self):
        return self.unit_price * self.quantity


if __name__ == '__main__':
    # Pierwszy test
    test_product = Product("shoes", 30, 3)
    print(test_product.get_price())

    # Trzeci test
    test_order = Order('maciej@example.com')
    print(test_order.customer_email)
    print(test_order.get_total_price())

    # Czwarty test

    test_order1 = Order('maciej@example.com')
    test_product1 = Product('Shoes', 30, 3)
    test_order1.add_product(test_product1)
    print(test_order1.get_total_price())

    # Piąty test, sprawdzam czy dla 3 produktow algorytm dziala

    test_product2 = Product("Shoes", 30, 3)
    test_product3 = Product("T-shirt", 50, 2)
    test_order1.add_product(test_product2)
    test_order1.add_product(test_product3)
    print(test_order1.get_total_price())

    # Szosty test, sprawdzam czy poprawnie wyswietla się liczba produktow

    print(test_order1.get_total_quantity_of_products())

    # 7 test, sprawdzam dzialanie funckcji purchase

    test_order1.purchase()
    print(test_order1.purchased)
